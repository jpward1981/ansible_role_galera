## [1.0.1](https://gitlab.com/jpward1981/ansible_role_galera/compare/v1.0.0...v1.0.1) (2019-11-19)


### Bug Fixes

* naming convention fix ([fe5b7de](https://gitlab.com/jpward1981/ansible_role_galera/commit/fe5b7de))

# 1.0.0 (2019-11-14)


### Bug Fixes

* Fix package dictionary in container test ([88b0ebd](https://gitlab.com/jpward1981/ansible_role_galera/commit/88b0ebd))
* Rename one handler to comply to naming ([af229f0](https://gitlab.com/jpward1981/ansible_role_galera/commit/af229f0))
